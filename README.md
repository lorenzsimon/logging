# Documentation of the Logging-Service

## What is the purpose of the Logging Service?
This service provides an internal API for all backend services to write the logs into.
Additional, usage statistics are generated from the added data.

> To visualise the data in influxDB, consider using the Grafana dashboard available at port 3000

## Internal Logic
The service is connected to an InfluxDB. This is a timeseries based database. The Service manages database connection, DB error handling, data structuring and request execution.

Contains ``QueryService.class`` that should be used as maven dependency for logging. This is a wrapper that handles all calls. 

## API
All endpoint routes are stored in the `...resources/application.properties`.

#### Baseparameters for every logging endpoint 
  - "service" (required): The service sending the log data. This is to separate logging from differnt services and service-usage data.
  - "time" (optional): The original time the log is generated. This improves ordering and avoids time-race conditions.

**`/log/userdata`**
- Purpose: Log data containing userdata
- Should only be accessed through wrapper QueryService.class
- Parameters: 
    - service: The service sending the log data
    - field,data pairs: the registered fields with data to log into it

**`/log/usage`**
- Purpose: Log data from application steps and error. Containing business logic and details
- Should only be accessed through wrapper QueryService.class
- Parameters: 
    - service: The service sending the log data
    - field,data pairs: the registered fields with data to log into it

**`/log/api`**
- Purpose: Log data from api accesses only
- Should only be accessed through wrapper QueryService.class
- Parameters: 
    - service: The service sending the log data
    - field,data pairs: the registered fields with data to log into it

**`/log/register`**
- Purpose: Registers tags and fields for a service. Logged data fields must be registered by every service.
- Should only be accessed through wrapper QueryService.class
- Parameters: 
    - service: The service executing the call
    - tags: List of tag names
    - fields: List of field names

**`/log/fields`**
- Purpose: Get registerd fields of a service
- Parameters: 
    - service: The service to get the fields from
  
**`/log/tags`**
- Purpose: Get registerd tags of a service
- Parameters: 
    - service: The service to get the tags from
  
**`/log/user/{userid}/queries/amount`**
- Purpose: Get the users amount of logged queries in a timespan
- Parameters: 
    - userid: The users ID
    - from (Optional): amount of days to go back to start the timespan from now. Default: 30
    - to (Optional): amount of days to go back to end the timespan form now: Default: 0
    