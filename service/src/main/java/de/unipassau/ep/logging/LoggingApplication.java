package de.unipassau.ep.logging;

import de.unipassau.ep.logging.database.InfluxConnection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

@SpringBootApplication
@EnableEurekaClient
public class LoggingApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoggingApplication.class, args);
    }

    @Bean
    @Scope("singleton")
    public InfluxConnection influxDB(@Value("${log.database.password}") String password,
                             @Value("${log.database.username}") String username,
                             @Value("${log.database.url}") String url) {
        return new InfluxConnection(password,username,url);
    }
}
