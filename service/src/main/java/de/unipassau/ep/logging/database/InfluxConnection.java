package de.unipassau.ep.logging.database;

import de.unipassau.ep.logging.database.enums.Database;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Pong;
import java.util.MissingResourceException;

/**
 * Wrapper for InfluxDB to centralise connection and settings.
 */
public class InfluxConnection {

    /** Instance Holder */
    private InfluxDB influxDB;

    /**
     * Creates a influx connection with error handling using provided credentials.
     * @param password Password of the Influx Database
     * @param username Username of the Influx Database
     * @param url URL of the Influx Database
     */
    public InfluxConnection(String password, String username, String url) {
        InfluxDB db = InfluxDBFactory.connect(url, username, password);
        Pong response = db.ping();
        if (response.getVersion().equalsIgnoreCase("unknown")) {
            System.out.println("ERROR: Cannot connect to database: " + url);
            System.out.println("  Using credentials: " + username + ", " + password);
            db.close();
            throw new MissingResourceException("Cannot connect to database: " + url, "Logging Database", "");
        }
        for (Database dbEnum : Database.values()) {
            db.createDatabase(dbEnum.getName());
            db.createRetentionPolicy(
                    "defaultPolicy", dbEnum.getName(), dbEnum.getDuration(), 1, true);
        }
        db.setLogLevel(InfluxDB.LogLevel.NONE);
        influxDB = db;
    }

    /**
     * Getter
     * @return Connected InfluxDB Object
     */
    public InfluxDB get() {
        return influxDB;
    }
}
