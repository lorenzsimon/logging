package de.unipassau.ep.logging.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriUtils;
import reactor.util.retry.Retry;

import java.time.Duration;

/**
 * A basic wrapper to communicate via Webclient
 * with basic Parameter and Environment Variables.
 * Now only used for Logging, can be extended to general usage
 */
public class LoggingService {

    @Value("${logging.baseURL:'http://service-logging:8088'}")
    private String baseURL = "http://service-logging:8088";

    private WebClient webClient;
    private Logger localLogger = LoggerFactory.getLogger(LoggingService.class);
    private String measurement;
    private boolean softRegistration = false;
    private MultiValueMap<String,String>fields = new LinkedMultiValueMap<>();

    /**
     * Creates an logging instance
     *
     * @param service The service name creating this instance.
     */
    public LoggingService(String service) {
        this(service, false);
    }

    public LoggingService(String service, boolean softRegistration) {
        this.webClient = WebClient.builder().baseUrl(baseURL).build();
        this.measurement = service;
        this.softRegistration = softRegistration;
    }

    /**
     * Registers tags on the logging service. This is required for later key identification and validation.
     * @param keys All keys that should get registered as tag (Non-Indexed)
     * @return this
     */
    public LoggingService registerTags(String[] keys) {
        this.webClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .pathSegment("log", "register")
                        .queryParam("service", measurement)
                        .queryParam("tags", keys)
                        .build())
                .retrieve()
                .bodyToMono(String.class)
                .retryWhen(Retry.fixedDelay(2, Duration.ofSeconds(5)))
                .doOnError((e) -> localLogger.error("Could not register tags: " + e.getMessage()))
                .subscribe();
        return this;
    }

    /**
     * Registers fields on the logging service. This is required for later key identification and validation.
     * @param keys All fields that should get registered as field (Indexed)
     * @return this
     */
    public LoggingService registerFields(String[] keys) {
        this.webClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .pathSegment("log", "register")
                        .queryParam("service", measurement)
                        .queryParam("fields", keys)
                        .build())
                .retrieve()
                .bodyToMono(String.class)
                .retryWhen(Retry.fixedDelay(2, Duration.ofSeconds(5)))
                .doOnError((e) -> localLogger.error("Could not register fields: " + e.getMessage()))
                .subscribe();
        return this;
    }

    /**
     * Adds a param to the logging data using a LoggingBuilder
     *
     * @param key   Key to search for
     * @param value value
     * @return LoggingService with new data
     */
    public LoggingBuilder add(String key, String value) {
        return new LoggingBuilder(key,value);
    }

    /**
     * Adds a param to the logging data using a LoggingBuilder
     *
     * @param key   Key to search for
     * @param value value
     * @return LoggingService with new data
     */
    public LoggingBuilder add(String key, long value) {
        return this.add(key, Long.toString(value));
    }

    /**
     * Helper class to keep persistance over LoggingService being a singleton
     */
    public class LoggingBuilder {
        private MultiValueMap<String,String>fields = new LinkedMultiValueMap<>();

        public LoggingBuilder(String key, String value) {
            value = value.replace("/", "-");
            value = UriUtils.encodeQueryParam(value, "UTF-8");

            fields.add(key, value);
        }


        public LoggingBuilder add(String key, String value) {
            value = value.replace("/", "-");
            value = UriUtils.encodeQueryParam(value, "UTF-8");

            fields.add(key, value);
            return this;
        }

        public LoggingBuilder add(String key, long value) {
            return this.add(key, Long.toString(value));
        }

        public LoggingBuilder add(String key, boolean value) {
            return this.add(key, Boolean.toString(value));
        }

        /**
         * Sends the data to the logging service
         *
         * @param type The type of logging data included. Defines the endpoint
         */
        public void send(String type) {
            fields.add("service", measurement);
            try {
                webClient
                        .get()
                        .uri(uriBuilder -> uriBuilder
                                .queryParams(fields)
                                .pathSegment("log", type)
                                .build())
                        .retrieve()
                        .bodyToMono(String.class)
                        .block();
            } catch (Exception e) {
                localLogger.error("Could not send data to the logging service: " + e.getMessage());
                localLogger.info("Logging data: " + fields.toString());
            } finally {
                this.fields.clear();
            }
        }

        /**
         * Sends the data to the logging service - default "usage" endpoint
         */
        public void send() {
            send("usage");
        }
    }
}