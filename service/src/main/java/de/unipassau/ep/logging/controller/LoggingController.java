package de.unipassau.ep.logging.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.unipassau.ep.logging.database.InfluxConnection;
import de.unipassau.ep.logging.database.enums.Database;
import de.unipassau.ep.logging.service.DBCols;
import org.influxdb.InfluxDB;
import org.influxdb.dto.BatchPoints;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
public class LoggingController {

    /** Influx Connection */
    private InfluxDB influxDB;
    /** Registered fields and tags for each service */
    private DBCols cols = new DBCols();

    @Autowired
    public void context(InfluxConnection context) {
        this.influxDB = context.get();
    }

    /**
     * Debug request
     * @return all data in userdata and usage influx databases
     */
    @GetMapping("/all")
    public String get() {
        QueryResult queryResult = influxDB.query(new Query("SELECT *", "userdata"));
        QueryResult queryResult2 = influxDB.query(new Query("SELECT *", "usage"));
        return "<p>" + queryResult.toString() + "</p> <p>" + queryResult2.toString() + "</p> ";
    }


    /**
     * Stores data to the userdata database. This is used for all user-related data and therefore security relvant!
     * @param service The service sending the log data
     * @param time Optional provided timestamp
     * @param userID The ID of the executing or related user
     * @param allRequestParams Key-Value pairs that are registered as tag or field and should be logged
     * @return HTTP Response
     */
    @GetMapping(value = {"${log.userdata}"})
    public ResponseEntity userdata(@RequestParam String service, @RequestParam(required = false) Long time, @RequestParam String userID, @RequestParam Map<String, String> allRequestParams) {
        if (time == null) {
            time = System.currentTimeMillis();
        }

        Point.Builder pointBuilder = Point.measurement(service)
                .addField("service", service)
                .addField("userID", userID);


        allRequestParams.remove("time");
        allRequestParams.remove("service");

        for (Map.Entry<String, String> entry : allRequestParams.entrySet()) {
            final String key = entry.getKey();
            final String value = entry.getValue();
            if (cols.isField(service, key)) {
                pointBuilder.addField(value, key);
            } else if (cols.isTag(service, key)) {
                pointBuilder.tag(key, value);
            } else {
                return ResponseEntity.badRequest().body("Key is not registered as tag nor value for this service!");
            }
        }
        BatchPoints bp = BatchPoints.database(Database.USERDATA.getName()).point(pointBuilder.build()).build();
        influxDB.write(bp);
        return ResponseEntity.ok().build();

    }


    /**
     * Stores data to the usage database. This is anonymous data to track the service usage & load.
     * @param service The service sending the log data
     * @param time Optional provided timestamp
     * @param allRequestParams Key-Value pairs that are registered as tag or field and should be logged
     * @return HTTP Response
     */
    @GetMapping(value = {"${log.usage}"})
    public ResponseEntity<? extends Object> usage(@RequestParam String service, @RequestParam(required = false) Long time, @RequestParam Map<String, String> allRequestParams) {
        if (time == null) {
            time = System.currentTimeMillis();
        }

        Point.Builder pointBuilder = Point.measurement(service)
                .time(time, TimeUnit.MILLISECONDS);

        allRequestParams.remove("time");
        allRequestParams.remove("service");

        for (Map.Entry<String, String> entry : allRequestParams.entrySet()) {
            final String key = entry.getKey();
            final String value = entry.getValue();

            if (cols.isField(service, key)) {
                pointBuilder.addField(value, key);
            } else if (cols.isTag(service, key)) {
                pointBuilder.tag(key, value);
            } else {
                return ResponseEntity.badRequest().body("Key " + key + " is not registered as tag nor value for service " + service + "!\n  Value sent " + value);
            }
        }
        BatchPoints bp = BatchPoints.database(Database.USAGE.getName()).point(pointBuilder.build()).build();
        influxDB.write(bp);
        return ResponseEntity.ok().build();
    }

    /**
     * Stores data to the api database. This is anonymous data to track the api usage & load.
     * @param service The service sending the log data
     * @param time Optional provided timestamp
     * @param allRequestParams Key-Value pairs that are registered as tag or field and should be logged
     * @return HTTP Response
     */
    @GetMapping(value = {"${log.api}"})
    public ResponseEntity<? extends Object> api(@RequestParam String service, @RequestParam(required = false) Long time, @RequestParam Map<String, String> allRequestParams) {
        if (time == null) {
            time = System.currentTimeMillis();
        }

        Point.Builder pointBuilder = Point.measurement(service)
                .time(time, TimeUnit.MILLISECONDS);

        allRequestParams.remove("time");
        allRequestParams.remove("service");

        for (Map.Entry<String, String> entry : allRequestParams.entrySet()) {
            final String key = entry.getKey();
            final String value = entry.getValue();

            if (cols.isField(service, key)) {
                pointBuilder.addField(value, key);
            } else if (cols.isTag(service, key)) {
                pointBuilder.tag(key, value);
            } else {
                return ResponseEntity.badRequest().body("Key " + key + " is not registered as tag nor value for service " + service + "!\n  Value sent " + value);
            }
        }
        BatchPoints bp = BatchPoints.database(Database.USAGE.getName()).point(pointBuilder.build()).build();
        influxDB.write(bp);
        return ResponseEntity.ok().build();
    }

    /**
     * Registered fields getter
     * @param service The service sending the request
     * @return All fields registered for the requested service
     */
    @GetMapping(value = {"${log.fields}"})
    public String getFields(@RequestParam String service) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonArray = null;
        try {
            jsonArray = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(cols.getFields(service));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    /**
     * Registered tags getter
     * @param service The service sending the request
     * @return All tags registered for the requested service
     */
    @GetMapping(value = {"${log.tags}"})
    public String getTags(@RequestParam String service) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonArray = null;
        try {
            jsonArray = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(cols.getTags(service));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    /**
     * Registeres tags and fields for a service. Mandatory for later logging requests!
     * @param service The service for that the keys will be registered for.
     * @param tags List of keys that get registered as a tag
     * @param fields List of fields that get registered as a field
     */
    @GetMapping(value = {"${log.register}"})
    public void register(@RequestParam String service, @RequestParam(required = false) List<String> tags, @RequestParam(required = false) List<String> fields) {
        if (tags != null) {
            for (String tag : tags) {
                cols.registerTag(service, tag);
            }
        }
        if (fields != null) {
            for (String field : fields) {
                cols.registerField(service, field);
            }
        }

    }
}
