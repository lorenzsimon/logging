package de.unipassau.ep.logging.database.pojo;

import lombok.Getter;
import org.influxdb.annotation.Column;
import org.springframework.beans.factory.annotation.Autowired;


public class Query {

    @Column(name = "service")
    @Getter
    private String service;

    @Column(name = "type")
    @Getter
    private String type;

    @Column(name = "query", tag = true)
    @Getter
    private String query;

}
