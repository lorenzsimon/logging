package de.unipassau.ep.logging.database.enums;

/**
 * Enum for all contained databases with retention policy durations.
 * This is iterated for database generation
 */
public enum Database {
    USERDATA("userdata","0s"), USAGE("usage","0s"), LOGS("api","0s");

    /** Database name */
    private String name;
    /** Retention duration */
    private String duration;

    Database(String name, String duration) {
        this.name = name;
        this.duration = duration;
    }

    public String getName() {
        return this.name;
    }

    public String getDuration() {
        return this.duration;
    }

}
