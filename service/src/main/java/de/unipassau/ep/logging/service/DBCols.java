package de.unipassau.ep.logging.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBCols {

    private Map<String,List<String>> serviceCols = new HashMap<>();

    /**
     * Registers a tag
     * @param service the executing service
     * @param name the tags name
     * @return success
     */
    public boolean registerTag(String service, String name) {
        if (!serviceCols.containsKey("tag-" + service)) {
            serviceCols.put("tag-" + service, new ArrayList<>());
        }
        List tags = serviceCols.get("tag-" + service);
        if (!tags.contains(name)) {
            return tags.add(name);
        }
        return false;
    }

    /**
     * Registers a field
     * @param service the executing service
     * @param name the fields name
     * @return success
     */
    public boolean registerField(String service, String name) {
        if (!serviceCols.containsKey("field-" + service)) {
            serviceCols.put("field-" + service, new ArrayList<>());
        }
        List tags = serviceCols.get("field-" + service);
        if (!tags.contains(name)) {
            return tags.add(name);
        }
        return false;
    }

    /**
     * If the name is registered as field in the service
     * @param service executing service
     * @param name field name
     * @return result
     */
    public Boolean isField(String service, String name) {
        return serviceCols.containsKey("field-" + service) && serviceCols.get("field-" + service).contains(name);
    }

    /**
     * If the name is registered as tag in the service
     * @param service executing service
     * @param name tag name
     * @return result
     */
    public Boolean isTag(String service, String name) {
        return serviceCols.containsKey("tag-" + service) && serviceCols.get("tag-" + service).contains(name);
    }

    /**
     * Getter for all fields of a service
     * @param service the executing service
     * @return result
     */
    public List<String> getFields(String service) {
        return serviceCols.get("field-" + service);
    }

    /**
     * Getter for all tags of a service
     * @param service the executing service
     * @return result
     */
    public List<String> getTags(String service) {
        return serviceCols.get("tag-" + service);
    }


}
